import { createDrawerNavigator } from 'react-navigation'

import HomeScreen from '../screens/Home';
import DrawerContentComponent from '../components/Drawer/DrawerContentComponent';

import SettingsStack from './SettingsStack';

const AuthDrawerNavigator = createDrawerNavigator({
  Home: {
    screen: HomeScreen,
    navigationOptions: {
      drawerLabel: 'My Home',
    },
  },
  Settings: {
    screen: SettingsStack,
    navigationOptions: {
      drawerLabel: 'Settings',
    },
  },
}, {
  contentComponent: DrawerContentComponent,
  initialRouteName: 'Home',
  drawerPosition: 'left',
  drawerWidth: 250,
});

export default AuthDrawerNavigator;
