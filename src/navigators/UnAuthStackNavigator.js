import { createStackNavigator } from 'react-navigation';

import LoginScreen from '../screens/Login';

const UnAuthStackNavigator = createStackNavigator({
  Login: {
    screen: LoginScreen,
    navigationOptions: {
      header: null,
    }
  },
}, {
  initialRouteName: 'Login',
});

export default UnAuthStackNavigator;
