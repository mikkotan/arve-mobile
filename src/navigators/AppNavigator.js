import { createSwitchNavigator } from 'react-navigation';

import AuthDrawerNavigator from './AuthDrawerNavigator';
import UnAuthStackNavigator from './UnAuthStackNavigator';

export const createRootNavigator = (isAuth = false) => (
  createSwitchNavigator({
    Authenticated: {
      screen: AuthDrawerNavigator,
    },
    UnAuthenticated: {
      screen: UnAuthStackNavigator,
    },
  }, {
    initialRouteName: isAuth ? 'Authenticated' : 'UnAuthenticated',
    headerMode: 'none',
  })
);
