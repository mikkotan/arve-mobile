import { createStackNavigator } from 'react-navigation';

import SettingsScreen from '../screens/Settings';
import DeviceListScreen from '../screens/DeviceList';

const SettingsStack = createStackNavigator({
  Settings: {
    screen: SettingsScreen,
    navigationOptions: {
      header: null,
    },
  },
  DeviceList: {
    screen: DeviceListScreen,
    navigationOptions: {
      header: null,
    },
  },
}, {
  initialRouteName: 'Settings',
});

export default SettingsStack;
