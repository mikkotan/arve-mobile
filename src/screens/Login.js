import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { Container, Content } from 'native-base';

import LoginForm from '../components/LoginForm';

import AuthAPI from '../api/authApi';
import AuthHelper from '../api/helpers/authHelper';
import DeviceHelper from '../api/helpers/deviceHelper';

class Login extends Component {
  constructor(props) {
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(payload) {
    AuthAPI.login(payload)
      .then(({ data }) => {
        AuthHelper.authenticate(data.auth_token);
        DeviceHelper.setMacAddress(data.mac);
        this.props.navigation.navigate('Authenticated')
      })
      .catch(err => alert(err));
  }

  render() {
    return (
      <Container>
        <Content padder contentContainerStyle={styles.root}>
          <LoginForm onSubmit={this.handleSubmit} />
        </Content>
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  form: {
    width: '80%'
  },
  button: {
    margin: 10,
    height: 30,
    backgroundColor: '#35b295'
  }
});

export default Login;
