import React from 'react';
import { StyleSheet } from 'react-native';
import {
  Body,
  Container,
  Content,
  Text,
  Left,
  List,
  ListItem,
  Right,
} from 'native-base';
import Icon from 'react-native-vector-icons/Ionicons';

import NavigationHeader from '../components/NavigationHeader';
import Switch from '../components/Switch';

const Settings = ({ navigation }) => (
  <Container>
    <NavigationHeader
      handlePress={navigation.toggleDrawer}
      headerTitle="Settings"
    />
    <Content>
      <List>
        <ListItem thumbnail onPress={() => navigation.navigate('DeviceList')}>
          <Left>
            <Icon name="md-flashlight" size={25} color="#8b8b8b" />
          </Left>
          <Body>
            <Text style={{ color: '#8b8b8b' }}>Connect your Arve System</Text>
          </Body>
        </ListItem>
        <ListItem thumbnail>
          <Left>
            <Icon name="md-power" size={25} color="#8b8b8b" />
          </Left>
          <Body>
            <Text style={{ color: '#8b8b8b' }}>Offline mode</Text>
            <Text note>
              Sets Arve 30 in autopilot mode and turns all radios off
            </Text>
          </Body>
          <Right>
            <Switch />
          </Right>
        </ListItem>
      </List>
    </Content>
  </Container>
);

const styles = StyleSheet.create({
  root: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default Settings;
