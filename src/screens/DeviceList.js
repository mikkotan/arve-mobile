import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import {
  Container,
  Content,
  Text,
  List,
  ListItem,
  Left,
  Body,
  View,
} from 'native-base';
import Icon from 'react-native-vector-icons/Ionicons';

import NavigationHeader from '../components/NavigationHeader';
import DeviceAPI from '../api/deviceApi';
import DeviceHelper from '../api/helpers/deviceHelper';

class DeviceList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      devices: [],
      selected: '',
    };
  }
  
  async componentDidMount() {
    const devices = await DeviceAPI.fetchDevices();
    const selected = await DeviceHelper.getMacAddress();
    
    this.setState({
      devices: devices.data,
      selected,
    });
  }

  handleSelect(mac) {
    DeviceHelper.setMacAddress(mac)
      .then(() => this.setState({ selected: mac}))
      .catch(err => alert(err));
  }

  render() {
    const { navigation } = this.props;
    const { devices } = this.state;
    const isSelected = (mac) => mac === this.state.selected;

    return (
      <Container>
        <NavigationHeader
          handlePress={() => navigation.goBack()}
          headerTitle="Connect your Arve System"
          iconName="md-arrow-back"
        />
        <Content>
          <View style={styles.noteRoot}>
            <Text style={styles.noteText}>
              Ensure your Arve device is connected to a power outlet and turned on. To turn the Arve 10 on press the power button on the back for 2 seconds.
            </Text>
            <Text style={styles.noteText}>
              Arve 10 is ready to connect when you see the Arve logo light blinking.
            </Text>
          </View>
          <Text style={styles.noteText}>
            Select the Arve device you want to connect:
          </Text>
          <List>
            {devices.map(({ mac_address }) => (
              <ListItem
                thumbnail
                key={mac_address}
                onPress={() => (
                  this.handleSelect(mac_address)
                )}
              >
                <Left>
                  <Icon
                    name="md-checkmark-circle-outline"
                    size={25}
                    color={isSelected(mac_address) ? 'green' : '#d3d3d3'} />
                </Left>
                <Body>
                  <Text>{mac_address}</Text>
                </Body>
              </ListItem>
            ))}
          </List>
        </Content>
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  noteRoot: {
    borderBottomWidth: 0.5,
    borderBottomColor: 'grey',
  },
  noteText: {
    marginTop: 10,
    marginBottom: 10,
    marginLeft: 15,
    marginRight: 15,
    color: 'black',
    fontWeight: '200',
    fontSize: 12
  },
});

export default DeviceList;
