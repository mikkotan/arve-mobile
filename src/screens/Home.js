import React, { Component, Fragment } from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import {
  Container,
  Content,
  Text,
  View,
} from 'native-base';
import SegmentedControlTab from 'react-native-segmented-control-tab';

import NavigationHeader from '../components/NavigationHeader';
import DeviceHelper from '../api/helpers/deviceHelper';
import DeviceAPI from '../api/deviceApi';

class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      device: null,
      selectedGraphView: 0,
    };
    this.handleSegmentChange = this.handleSegmentChange.bind(this);
  };

  async componentDidMount() {
    const mac = await DeviceHelper.getMacAddress();
    const device = await DeviceAPI.fetchDevice(mac);

    this.setState({
      device: device.data,
    });
  }

  handleSegmentChange(index) {
    this.setState({
      selectedGraphView: index,
    });
  }

  render() {
    const { device, selectedGraphView } = this.state;
    const { navigation } = this.props;

    return (
      <Container>
        <NavigationHeader
          handlePress={navigation.toggleDrawer}
          headerTitle="My Home"
        />
        <Content contentContainerStyle={styles.root}>
          {device && (
            <View style={styles.deviceContainer}>
              <View style={styles.deviceLeft}>
                <Text style={styles.label}>Home</Text>
                <View elevation={5} style={styles.circleInner}>
                  <Fragment>
                    <Text style={styles.mediumText}>
                      AQI
                    </Text>
                    <Text style={styles.intAQI}>
                      {device.int_aqi}
                    </Text>
                    <Text style={styles.mediumText}>
                      Moderate
                    </Text>
                  </Fragment>
                </View>
              </View>
              <View style={styles.deviceRight}>
                <Text style={styles.label}>
                  {device.city}, {device.country}
                </Text>
                <View style={styles.extAQIContainer}>
                  <Text style={[styles.mediumText, { color: '#4a9866'}]}>
                    AQI
                  </Text>
                  <Text style={styles.extAQI}>32</Text>
                  <Text style={styles.extIndicator}>Good</Text>
                  <Text note>Today at 3:43 PM</Text>
                </View>
              </View>
            </View>
          )}
          <View style={styles.graphContainer}>
            <View style={styles.segmentContainer}>
              <SegmentedControlTab
                values={['MINUTES', 'HOURS', 'DAYS']}
                selectedIndex={selectedGraphView}
                onTabPress={this.handleSegmentChange}
                borderRadius={0}
                tabStyle={{ borderWidth: 0, borderBottomWidth: 2, borderBottomColor: '#e6e6e6' }}
                tabTextStyle={{ color: '#81cdbc' }}
                activeTabStyle={{ backgroundColor: 'white', borderBottomWidth: 2, borderBottomColor: '#35b295' }}
                activeTabTextStyle={{ color: '#4a9866' }}
              />
            </View>
          </View>
        </Content>
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  deviceContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  deviceLeft: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    right: -30,
  },
  deviceRight: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    left: -30,
  },
  graphContainer: {
    flex: 2,
    backgroundColor: 'cyan',
  },
  circleInner: {
    borderWidth: 5,
    borderRadius: 60,
    borderColor: 'white',
    backgroundColor: '#ffe03a',
    width: 120,
    height: 120,
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: 'red',
    shadowRadius: 5,
    shadowOpacity: 0.75,
    shadowOffset: { width: 0, height: 0 },
    margin: 20,
  },
  intAQI: {
    fontWeight: 'bold',
    fontSize: 27,
  },
  extAQI: {
    fontWeight: 'bold',
    fontSize: 24,
    color: '#4a9866',
  },
  extIndicator: {
    color: '#4a9866',
    fontWeight: '500',
  },
  label: {
    color: '#bcbcbc',
    fontSize: 12,
    fontWeight: '500',
    top: 12,
  },
  mediumText: {
    fontSize: 12,
    fontWeight: '400',
  },
  extAQIContainer: {
    width: 120,
    height: 120,
    alignItems: 'flex-start',
    justifyContent: 'center',
    alignSelf: 'center',
    margin: 20,
  },
  segmentContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    width: '100%',
    bottom: 5,
  },
  activeTabStyle: {
    backgroundColor: '#D52C43',
  },
  tabStyle: {
    borderColor: '#D52C43',
  },
});

export default Home;
