import React, { Component } from 'react';
import { Switch } from 'react-native';

class CustomSwitch extends Component {
  constructor() {
    super();

    this.state = {
      value: false,
    };
  }

  render() {
    const { value } = this.state;

    return (
      <Switch
        value={value}
        trackColor="#35b295"
        onValueChange={value => this.setState({ value })}
      />
    )
  }
};

export default CustomSwitch;
