import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/Ionicons';

import { StyleSheet, TouchableOpacity } from 'react-native';
import {
  Header,
  Left,
  Body,
  Title,
} from 'native-base';

const NavigationHeader = ({
  handlePress,
  headerTitle,
  iconName,
}) => (
  <Header style={styles.header}>
    <Left>
      <TouchableOpacity onPress={handlePress}>
        <Icon name={iconName} color="white" size={22} />
      </TouchableOpacity>
    </Left>
    <Body style={styles.body}>
      <Title>{headerTitle}</Title>
    </Body>
  </Header>
);

NavigationHeader.propTypes = {
  handlePress: PropTypes.func.isRequired,
  headerTitle: PropTypes.string.isRequired,
};

NavigationHeader.defaultProps = {
  iconName: "ios-menu",
};

const styles = StyleSheet.create({
  header: {
    backgroundColor: '#35b295',
  },
  body: {
    flex: 3,
  },
});

export default NavigationHeader;
