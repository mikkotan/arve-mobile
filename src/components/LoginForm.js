import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet } from 'react-native';
import {
  Button,
  Form,
  Item,
  Input,
  Text,
} from 'native-base';

class LoginForm extends Component {
  constructor(props) {
    super(props)

    this.state = {
      email: '',
      password: '',
    };
  }

  render() {
    const { email, password } = this.state;
    const { onSubmit } = this.props;

    return (
      <Form style={styles.form}>
        <Item inlineLabel last>
          <Input
            placeholder="Email"
            value={email}
            onChangeText={email => (
              this.setState({ email })
            )}
          />
        </Item>
        <Item inlineLabel last>
          <Input
            secureTextEntry={true}
            placeholder="Password"
            value={password}
            onChangeText={password => (
              this.setState({ password })
            )}
          />
        </Item>
        <Button
          block
          style={styles.button}
          onPress={() => onSubmit(this.state)}
        >
          <Text>Login</Text>
        </Button>
      </Form>
    )
  }
}

LoginForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
};

const styles = StyleSheet.create({
  form: {
    width: '80%',
  },
  button: {
    margin: 10,
    height: 30,
    backgroundColor: '#35b295',
  },
});

export default LoginForm;
