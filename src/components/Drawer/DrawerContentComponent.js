import React from 'react';
import { StyleSheet } from 'react-native';
import { Content, Container, Body, Header, Text, List } from 'native-base';

import DrawerItem from './DrawerItem';

import AuthHelper from '../../api/helpers/authHelper';

const ContentComponent = props => (
  <Container>
    <Header style={styles.header}>
      <Body>
        <Text style={styles.text}>Menu</Text>
      </Body>
    </Header>
    <Content>
      <List>
        <DrawerItem
          label="My Home"
          iconName="ios-home"
          onPress={() => props.navigation.navigate('Home')}
        />
        <DrawerItem
          label="Air Quality Sensors"
          iconName="ios-snow"
          onPress={() => props.navigation.navigate('Settings')}
        />
        <DrawerItem
          label="Turbine Control"
          iconName="ios-cog"
          onPress={() => props.navigation.navigate('Settings')}
        />
        <DrawerItem
          label="Light Control"
          iconName="ios-sunny"
          onPress={() => props.navigation.navigate('Settings')}
        />
        <DrawerItem
          label="Settings"
          iconName="ios-settings"
          onPress={() => props.navigation.navigate('Settings')}
        />
        <DrawerItem
          label="Log out"
          iconName="ios-log-out"
          customStyles={{ borderBottomWidth: 0 }}
          onPress={() => {
            AuthHelper.deauthenticate()
              .then(() => props.navigation.navigate('UnAuthenticated'))
              .catch(err => alert('Something went wrong'));
          }}
        />
      </List>
    </Content>
  </Container>
);

const styles = StyleSheet.create({
  header: {
    backgroundColor: '#35b295',
  },
  text: {
    color: 'white',
  },
});

export default ContentComponent;
