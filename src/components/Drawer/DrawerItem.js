import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/Ionicons';

import { StyleSheet, TouchableOpacity } from 'react-native';
import { Body, Left, Text } from 'native-base';

const DrawerItem = ({
  label,
  iconName,
  onPress,
  customStyles,
}) => (
  <TouchableOpacity style={styles.drawerItem} onPress={onPress}>
    <Left style={styles.left}>
      <Icon name={iconName} size={25} color="grey" />
    </Left>
    <Body style={[ styles.body, customStyles ]}>
      <Text style={styles.drawerLabel}>
        {label}
      </Text>
    </Body>
  </TouchableOpacity>
);

DrawerItem.propTypes = {
  label: PropTypes.string.isRequired,
  iconName: PropTypes.string.isRequired,
  onPress: PropTypes.func,
  customStyles: PropTypes.objectOf(PropTypes.any),
};

DrawerItem.defaultProps = {
  onPress: null,
};

const styles = StyleSheet.create({
  drawerItem: {
    flex: 1,
    flexDirection: 'row',
  },
  left: {
    marginLeft: 15,
    flex: 1,
  },
  body: {
    flex: 3,
    borderBottomWidth: 0.5,
    borderBottomColor: 'grey',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    paddingTop: 15,
    paddingBottom: 15,
  },
  drawerLabel: {
    color: 'grey',
  },
});

export default DrawerItem;
