import { Platform } from 'react-native';
import axios from 'axios';

import AuthHelper from './helpers/authHelper';
import { BASE_URL, APP_CREDENTIALS } from './helpers/apiConfigHelper';

const login = ({ email, password}) => (
  axios.post(`${BASE_URL}/authenticate`, {
    ...APP_CREDENTIALS,
    member: {
      email,
      password,
    },
    device:{
      id: '123123',
      platform: Platform.OS
    }
  }, {
    headers: AuthHelper.getHeaders(),
  })
);

export default {
  login,
}
