import { AsyncStorage } from 'react-native';

const authenticate = token => AsyncStorage.setItem('token', token);
const deauthenticate = () => AsyncStorage.removeItem('token');
const getToken = () => AsyncStorage.getItem('token');
const getHeaders = () => ({
  'Content-Type': 'application/json',
});
const isAuthenticated = async () => {
  const token = await AsyncStorage.getItem('token');

  return token !== null;
};

export default {
  authenticate,
  deauthenticate,
  getToken,
  getHeaders,
  isAuthenticated,
};
