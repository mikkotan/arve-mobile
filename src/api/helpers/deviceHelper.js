import { AsyncStorage } from 'react-native';

const setMacAddress = mac_address => AsyncStorage.setItem('mac', mac_address);
const getMacAddress = () => AsyncStorage.getItem('mac');

export default {
  setMacAddress,
  getMacAddress,
};