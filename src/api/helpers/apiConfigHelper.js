import Config from 'react-native-config';

export const BASE_URL = Config.API_URL;

export const APP_CREDENTIALS = {
  app_id: Config.APP_ID,
  app_key: Config.APP_KEY,
};
