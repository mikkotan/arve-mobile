import axios from 'axios';

import AuthHelper from './helpers/authHelper';
import DeviceHelper from './helpers/deviceHelper';
import { BASE_URL, APP_CREDENTIALS } from './helpers/apiConfigHelper';

const fetchDevices = () => (
  axios.get(`${BASE_URL}/devices`, {
    headers: AuthHelper.getHeaders(),
  })
);

const fetchDevice = mac_address => (
  axios.get(`${BASE_URL}/devices/${mac_address}`, {
    headers: AuthHelper.getHeaders(),
  })
);

const postControlCommand = async ({
  light,
  motor,
  light_auto,
  motor_auto,
}) => {
  const mac = await DeviceHelper.getMacAddress();
  const url = `${BASE_URL}/devices/${mac.toUpperCase()}/commands`;

  return axios.post(url, {
    ...APP_CREDENTIALS,
    command: {
      data: {
        light_intensity: light,
        motor_rpm: motor,
        motor_rpm_autopilot: motor_auto === true ? 1: 0,
        light_intensity_autopilot: light_auto === true ? 1 : 0,
      }
    }
  })
};

const getAqiHistory = async () => {
  const mac = await DeviceHelper.getMacAddress();
  const url = `${BASE_URL}/devices/${mac.toUpperCase()}/aqi_history`;

  return axios.get(url, {
    headers: AuthHelper.getHeaders(),
  });
};

const getTurbineRPMHistory = async () => {
  const mac = await DeviceHelper.getMacAddress();
  const url = `${BASE_URL}/devices/${mac.toUpperCase()}/motor_rpm_history`;

  return axios.get(url, {
    headers: AuthHelper.getHeaders(),
  });
};

export default {
  fetchDevices,
  fetchDevice,
  postControlCommand,
  getAqiHistory,
  getTurbineRPMHistory,
};