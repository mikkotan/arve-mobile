import React, { Component } from 'react';
import { createRootNavigator } from './src/navigators/AppNavigator';

import AuthHelper from './src/api/helpers/authHelper';

class App extends Component {
  constructor() {
    super();

    this.state = {
      authenticated: false,
      checkedAuth: false,
    };
  }

  componentDidMount() {
    AuthHelper.isAuthenticated()
      .then(res => {
        this.setState({
          authenticated: res,
          checkedAuth: true,
        });
      })
      .catch(err => alert("An error occured."));
  }

  render() {
    const { checkedAuth, authenticated } = this.state;
    const AppNavigator = createRootNavigator(authenticated);
    
    if (!checkedAuth) {
      return null;
    }

    return <AppNavigator />
  }
}

export default App;